#include <GLFramework.h>

const GLchar *vertex_shader_source = {
	"#version 330 core\n"
	"\n"
	"void main(void)\n"
	"{\n"
	"\t""gl_Position = vec4(0.0, 0.0, 0.5, 1.0);\n"
	"}"
};
const GLchar *fragment_shader_source = {
	"#version 330 core\n"
	"\n"
	"out vec4 color;\n"
	"\n"
	"void main(void)\n"
	"{\n"
	"\t""color = vec4(0.0, 0.0, 1.0, 1.0);\n"
	"}"
};


int main(void)
{
	GLFramework first;
	GLFWwindow *window = first.window;

	double deltaSec = 0;
	double prevTime = 0;
	double currTime = 0;

//	GLuint program = GLFramework::CompileShaders(
//		GLFramework::LoadSource("vs.glsl").c_str(),
//		GLFramework::LoadSource("fs.glsl").c_str()
//		);
	GLuint program = GLFramework::LoadShaders(
		"vs.glsl", "fs.glsl");

	glPointSize(40.0f);
	while(!glfwWindowShouldClose(window))
	{
		prevTime = currTime;
		currTime = glfwGetTime();
		deltaSec = currTime - prevTime;

		const GLfloat color[] = {
			(float)sin(currTime) * 0.5f + 0.5f,
			(float)cos(currTime) * 0.5f + 0.5f,
			0.0f, 1.0f
		};
		glClearBufferfv(GL_COLOR, 0, color);

		glUseProgram(program);
		glDrawArrays(GL_POINTS, 0, 1);

		glfwSwapBuffers(window);
		glfwPollEvents();
		
		if(glfwGetKey(window, GLFW_KEY_ESCAPE))
			glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

	glDeleteProgram(program);

	return 0;
}
