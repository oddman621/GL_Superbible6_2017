#include "../GLFramework/GLFramework.h"

int main(void)
{
	GLFramework fw;
	GLFWwindow *const window = fw.window;
	GLuint const &vao = fw.vao;

	double deltaSec = 0;
	double prevTime = 0;
	double currTime = 0;

	const std::string
		vs_src = GLFramework::LoadSource("vs.glsl"),
		fs_src = GLFramework::LoadSource("fs.glsl");
//		tcs_src = GLFramework::LoadSource("tcs.glsl"),
//		tes_src = GLFramework::LoadSource("tes.glsl"),
//		gs_src = GLFramework::LoadSource("gs.glsl"),
	GLuint shaders[] = {
		GLFramework::CompileShader(vs_src.c_str(), GL_VERTEX_SHADER),
		GLFramework::CompileShader(fs_src.c_str(), GL_FRAGMENT_SHADER),
//		GLFramework::CompileShader(tcs_src.c_str(), GL_TESS_CONTROL_SHADER),
//		GLFramework::CompileShader(tes_src.c_str(), GL_TESS_EVALUATION_SHADER),
//		GLFramework::CompileShader(gs_src.c_str(), GL_GEOMETRY_SHADER),

	};
	const int shadersArraySize = sizeof(shaders)/sizeof(*shaders);
		
	GLuint program = GLFramework::CompileProgram(
		shadersArraySize, shaders);

	const GLfloat triangle_vertex[] = {
		0.25f, -0.25f, 0.5f,
		-0.25f, -0.25f, 0.5f,
		0.25f, 0.25f, 0.5f,
	};
	const GLfloat triangle_color[] = {
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
	};
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle_vertex),
				 NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0,
					sizeof(triangle_vertex), triangle_vertex);
	GLuint buffer2;
	glGenBuffers(1, &buffer2);
	glBindBuffer(GL_ARRAY_BUFFER, buffer2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle_color),
				 NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0,
					sizeof(triangle_color), triangle_color);
	
	while(!glfwWindowShouldClose(window))
	{
		prevTime = currTime;
		currTime = glfwGetTime();
		deltaSec = currTime - prevTime;

		const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		glClearBufferfv(GL_COLOR, 0, black);

		glUseProgram(program);

		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, buffer2);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(1);

		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CCW);
		glCullFace(GL_FRONT);

		glDrawArrays(GL_TRIANGLES, 0, 3);
		glfwSwapBuffers(window);
		glfwPollEvents();
		
		if(glfwGetKey(window, GLFW_KEY_ESCAPE))
			glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

	glDeleteProgram(program);
	for(int i=0; i<shadersArraySize; i++)
		glDeleteShader(shaders[i]);

	return 0;
}
