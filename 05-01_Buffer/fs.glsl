#version 420 core

in vec3 vs_color;
out vec4 color;

void main(void)
{
//	color = vec4(0.8, 0.0, 0.0, 1.0);
	color = vec4(vs_color, 1.0);
}
