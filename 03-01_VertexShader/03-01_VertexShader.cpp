#include <GLFramework.h>

int main(void)
{
	GLFramework first;
	GLFWwindow *window = first.window;

	double deltaSec = 0;
	double prevTime = 0;
	double currTime = 0;

	GLuint program = GLFramework::LoadShaders(
		"vs.glsl", "fs.glsl");

	while(!glfwWindowShouldClose(window))
	{
		prevTime = currTime;
		currTime = glfwGetTime();
		deltaSec = currTime - prevTime;

		const GLfloat color[] = {
			(float)sin(currTime) * 0.5f + 0.5f,
			(float)cos(currTime) * 0.5f + 0.5f,
			0.0f, 1.0f
		};
		glClearBufferfv(GL_COLOR, 0, color);

		glUseProgram(program);

		const GLfloat attrib[4] = {
			(float)sin(currTime) * 0.5f,
			(float)cos(currTime) * 0.6f,
			0.0f, 0.0f
		};
		glVertexAttrib4fv(0, attrib);

		glDrawArrays(GL_TRIANGLES, 0, 3);
		
		glfwSwapBuffers(window);
		glfwPollEvents();
		
		if(glfwGetKey(window, GLFW_KEY_ESCAPE))
			glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

	glDeleteProgram(program);

	return 0;
}
