//#include <GLFramework.h>
#include "../GLFramework/GLFramework.h"

int main(void)
{
	GLFramework first;
	GLFWwindow *window = first.window;

	double deltaSec = 0;
	double prevTime = 0;
	double currTime = 0;

	const std::string
		vs_src = GLFramework::LoadSource("vs.glsl"),
		fs_src = GLFramework::LoadSource("fs.glsl"),
		tcs_src = GLFramework::LoadSource("tcs.glsl"),
		tes_src = GLFramework::LoadSource("tes.glsl");
	GLuint shaders[] = {
		GLFramework::CompileShader(vs_src.c_str(), GL_VERTEX_SHADER),
		GLFramework::CompileShader(fs_src.c_str(), GL_FRAGMENT_SHADER),
		GLFramework::CompileShader(tcs_src.c_str(), GL_TESS_CONTROL_SHADER),
		GLFramework::CompileShader(tes_src.c_str(), GL_TESS_EVALUATION_SHADER),

	};
	const int shadersArraySize = sizeof(shaders)/sizeof(*shaders);
		
	GLuint program = GLFramework::CompileProgram(
		shadersArraySize, shaders);
	while(!glfwWindowShouldClose(window))
	{
		prevTime = currTime;
		currTime = glfwGetTime();
		deltaSec = currTime - prevTime;

		const GLfloat color[] = {
			(float)sin(currTime) * 0.5f + 0.5f,
			(float)cos(currTime) * 0.5f + 0.5f,
			0.0f, 1.0f
		};
		const GLfloat white[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		glClearBufferfv(GL_COLOR, 0, black);

		glUseProgram(program);

		const GLfloat attrib[4] = {
			(float)sin(currTime) * 0.5f,
			(float)cos(currTime) * 0.6f,
			0.0f, 0.0f
		};
		
		glVertexAttrib4fv(0, attrib);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDrawArrays(GL_PATCHES, 0, 3);
//		glPointSize(40.0f);
//		glDrawArrays(GL_POINT, 0, 3);
		glfwSwapBuffers(window);
		glfwPollEvents();
		
		if(glfwGetKey(window, GLFW_KEY_ESCAPE))
			glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

	glDeleteProgram(program);
	for(int i=0; i<shadersArraySize; i++)
		glDeleteShader(shaders[i]);

	return 0;
}
