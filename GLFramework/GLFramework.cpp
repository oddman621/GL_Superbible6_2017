#include "GLFramework.h"

GLFramework::GLFramework()
{
	if(!glfwInit())
	{
		printf("Failed to initialize glfw3!\n");
	}
	else
	{
		GLFWmonitor *monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode *mode =
			glfwGetVideoMode(monitor);
		
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		mainWindow = glfwCreateWindow(mode->width, mode->height,
									  "GL Framework", monitor, NULL);
		if(!mainWindow)
		{
			printf("Failed to create window!\n");
		}
		else
		{
			glfwMakeContextCurrent(window);
			if(glewInit() != GLEW_OK)
			{
				printf("Failed to initialize glew!\n");
			}
			else
			{
				glGenVertexArrays(1, &_vao);
				glBindVertexArray(_vao);
			}
		}
	}
}

GLFramework::~GLFramework()
{
	if(mainWindow) glfwDestroyWindow(mainWindow);
	mainWindow = NULL;
	if(_vao) glDeleteVertexArrays(1, &_vao);
	_vao = 0;
	glfwTerminate();
}

GLuint GLFramework::CompileProgram(int size,
								   GLuint *shaders)
{
	GLuint program = glCreateProgram();
	for(int i=0; i<size; i++)
	{
		glAttachShader(program, shaders[i]);
	}
	glLinkProgram(program);
	debug_program(program);
	for(int i=0; i<size; i++)
	{
		glDetachShader(program, shaders[i]);
	}
	return program;
}

GLuint GLFramework::CompileShader(const char *src,
								  GLenum type)
{
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &src, NULL);
	glCompileShader(shader);
	debug_shader(shader);
	return shader;
}

void GLFramework::debug_program(GLuint program)
{
	int link_status = 0, info_log_length = 0, attached_shaders = 0;
	std::string status;
	glGetProgramiv(program, GL_LINK_STATUS, &link_status);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
	glGetProgramiv(program, GL_ATTACHED_SHADERS, &attached_shaders);

	GLchar *message = new GLchar[info_log_length];

	switch(link_status)
	{
	case GL_TRUE: status = "Linking program succeeded."; break;
	case GL_FALSE: status = "Failed linking program!"; break;
	default: status = "UNKNOWN"; break;
	}

	std::cout << "Program: " << status.c_str() << std::endl;

	if(link_status == GL_FALSE)
	{
		glGetProgramInfoLog(program, info_log_length,
							&info_log_length, message);
		std::cout << message << std::endl;
	}

	delete[] message;
}

void GLFramework::debug_shader(GLuint shader)
{
	int shader_type = 0,
		compile_status = 0,
		info_log_length = 0;
//	char *type_name = NULL, *status = NULL;
	std::string type_name, status;
	glGetShaderiv(shader, GL_SHADER_TYPE, &shader_type);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_status);
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);

//	GLchar *message[info_log_length];
	GLchar *message = new GLchar[info_log_length];

	switch(shader_type)
	{
	case 0: type_name = "ERROR"; break;
	case GL_VERTEX_SHADER: type_name = "Vertex Shader"; break;
	case GL_FRAGMENT_SHADER: type_name = "Fragment Shader"; break;
	case GL_TESS_CONTROL_SHADER: type_name = "Tessellation Control Shader"; break;
	case GL_TESS_EVALUATION_SHADER: type_name = "Tessellation Evaluation Shader"; break;
	case GL_GEOMETRY_SHADER: type_name = "Geometry Shader"; break;
//	case GL_COMPUTE_SHADER: type_name = "Compute Shader"; break;
	default: type_name = "UNKNOWN"; break;
	}

	switch(compile_status)
	{
	case GL_TRUE: status = "Successfully compiled."; break;
	case GL_FALSE: status = "Failed to compile!"; break;
	default: status = "Unknown error"; break;
	}

	std::cout << type_name.c_str() << ": " << status.c_str() << std::endl;
	if(compile_status == GL_FALSE)
	{
		glGetShaderInfoLog(shader, info_log_length,
						   &info_log_length, message);
		std::cout << message << std::endl;
	}
	delete[] message;
}

std::string GLFramework::LoadSource(const char *path)
{
	std::fstream file(path);
	if(!file.is_open())
	{
		std::cout << "GLFramework::LoadSource() : "
			"File could not be opened." << std::endl;
	}

	return std::string( std::istreambuf_iterator<char>(file),
						std::istreambuf_iterator<char>()
		);
}

void GLFramework::LoadOBJ(const char *path,
						  std::vector<glm::vec4> *vertex,
						  std::vector<glm::vec3> *texuv,
						  std::vector<glm::vec3> *normal)
{
	std::ifstream file(path, std::ios::in);
	if(!file.is_open())
	{
		std::cout << "Failed to open Wavefront OBJ file : "
				  << path << std::endl;
		return;
	}

	//init parameters
	if(!vertex) vertex = new std::vector<glm::vec4>;
	else normal->clear();
	if(!texuv) texuv = new std::vector<glm::vec3>;
	else texuv->clear();
	if(!normal) normal = new std::vector<glm::vec3>;
	else normal->clear();

	std::string line;
	while(getline(file, line))
	{
		std::istringstream s(line);
		std::string header; s>>header;
		if(header == "#") continue;

		if(header == "v")
		{
			glm::vec4 v; s>>v.x>>v.y>>v.z; v.w=1.0f;
			vertex->push_back(v);
			continue;
		}
		if(header == "vt")
		{
			glm::vec3 vt; s>>vt.x>>vt.y; vt.z=0.0f;
			texuv->push_back(vt);
			continue;
		}
		if(header == "vn")
		{
			glm::vec3 vn; s>>vn.x>>vn.y>>vn.z;
			normal->push_back(vn);
			continue;
		}
	}
}

void render_object::LoadOBJ(const char *path)
{
	GLFramework::LoadOBJ(path, &vertices, &uvs, &normals);

	
}

render_object::render_object()
{}
render_object::render_object(const char *path)
{
	LoadOBJ(path);
}
render_object::~render_object()
{
	if(program) glDeleteProgram(program);
}

void render_object::render()
{}
