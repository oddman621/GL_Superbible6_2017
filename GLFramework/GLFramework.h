#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

class GLFramework
{
private:
	GLFWwindow *mainWindow = NULL;
	GLuint _vao = 0;
protected:
	static void debug_shader(GLuint shader);
	static void debug_program(GLuint program);
public:
	GLFWwindow *const &window = mainWindow;
	GLuint const &vao = _vao;
	GLFramework();
	virtual ~GLFramework();

	static std::string LoadSource(const char *path);
	static GLuint CompileShader(const char *src,
								GLenum type);
	static GLuint CompileProgram(int size,
								 GLuint *shaders);

	//Spec of obj wavefront file:
	//vertex : v(xyzw), vt(uvw), vp(uvw), vn(ijk)
	//.. and etc.
	//2017-05-27 I don't use 'mtl' right now.
	static void LoadOBJ(const char *path,
						std::vector <glm::vec4> *vertex,
						std::vector <glm::vec3> *texuv,
						std::vector <glm::vec3> *normal);
};

class render_object
{
public:
	GLuint program;
	GLuint vertexShader, fragmentShader;
	GLuint vertexBuffer, uvBuffer, normalBuffer;
protected:
	std::vector<glm::vec4> vertices;
	std::vector<glm::vec3> uvs;
	std::vector<glm::vec3> normals;
public:
	void LoadOBJ(const char *path);
	void render();
public:
	render_object();
	render_object(const char *path);
	virtual ~render_object();
};
