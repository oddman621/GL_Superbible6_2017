#include "../GLFramework/GLFramework.h"


const GLfloat triangle_pos[] = {
	0.25f, -0.25f, 0.5f,
	-0.25f, -0.25f, 0.5f,
	0.25f, 0.25f, 0.5f,
};
const GLfloat triangle_color[] = {
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 1.0f,
};

struct pos3f{
	GLfloat x=0, y=0, z=0;
};
struct color3f{
	GLfloat r=0, g=0, b=0;
};
struct vertex{
	pos3f pos;
	color3f color;
};

const vertex triangle_vertices[] = {
	{triangle_pos[0], triangle_pos[1], triangle_pos[2],
	 triangle_color[0], triangle_color[1], triangle_color[2]},
	{triangle_pos[3], triangle_pos[4], triangle_pos[5],
	 triangle_color[3], triangle_color[4], triangle_color[5]},
	{triangle_pos[6], triangle_pos[7], triangle_pos[8],
	 triangle_color[6], triangle_color[7], triangle_color[8]},
};


int main(void)
{
	GLFramework fw;
	GLFWwindow *const window = fw.window;
	GLuint const &vao = fw.vao;

	double deltaSec = 0;
	double prevTime = 0;
	double currTime = 0;

	const std::string
		vs_src = GLFramework::LoadSource("vs.glsl"),
		fs_src = GLFramework::LoadSource("fs.glsl");
	GLuint shaders[] = {
		GLFramework::CompileShader(vs_src.c_str(), GL_VERTEX_SHADER),
		GLFramework::CompileShader(fs_src.c_str(), GL_FRAGMENT_SHADER),

	};
	const int shadersArraySize = sizeof(shaders)/sizeof(*shaders);
		
	GLuint program = GLFramework::CompileProgram(
		shadersArraySize, shaders);

	///user init start
	glUseProgram(program);
//	glUniform3fv(17, 3, triangle_pos);
	glUseProgram(0);
 	///user init end

	while(!glfwWindowShouldClose(window))
	{
		prevTime = currTime;
		currTime = glfwGetTime();
		deltaSec = currTime - prevTime;

		const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		glClearBufferfv(GL_COLOR, 0, black);

		glUseProgram(program);

		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CCW);
		glCullFace(GL_FRONT);

		glDrawArrays(GL_TRIANGLES, 0, 3);
		glfwSwapBuffers(window);
		glfwPollEvents();
		
		if(glfwGetKey(window, GLFW_KEY_ESCAPE))
			glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

	glDeleteProgram(program);
	for(int i=0; i<shadersArraySize; i++)
		glDeleteShader(shaders[i]);

	return 0;
}
