#version 420 core

//layout (location = 18) uniform vec3 uniform_color;
layout(std140) uniform TransformBlock
{
	float scale;
	vec3 translation;
	float rotation[3];
	mat4 projection_matrix;
}

out vec4 color;

void main(void)
{
//	color = vec4(0.8, 0.0, 0.0, 1.0);
//	color = vec4(uniform_color, 1.0);
}
