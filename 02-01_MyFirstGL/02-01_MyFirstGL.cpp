#include <GLFramework.h>

int main(void)
{
	GLFramework first;
	GLFWwindow *window = first.window;

	double deltaSec = 0;
	double prevTime = 0;
	double currTime = 0;
	
	
	while(!glfwWindowShouldClose(window))
	{
		prevTime = currTime;
		currTime = glfwGetTime();
		deltaSec = currTime - prevTime;

		const GLfloat color[] = {
			(float)sin(currTime) * 0.5f + 0.5f,
			(float)cos(currTime) * 0.5f + 0.5f,
			0.0f, 1.0f
		};
		static const GLfloat red[] = {0.5f, 0.0f, 0.0f, 1.0f};
		glClearBufferfv(GL_COLOR, 0, red);

		glfwSwapBuffers(window);
		glfwPollEvents();
		
		if(glfwGetKey(window, GLFW_KEY_ESCAPE))
			glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}
