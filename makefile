LIBS = -lGL -lGLEW -lglfw -lGLFramework
INCLUDE = -IGLFramework
LIB = -LGLFramework
CC = g++
COMPILE_ORDER = $(CC) $(INCLUDE) -c $^ -o $@
LINK_ORDER = $(CC) $^ $(INCLUDE) $(LIB) $(LIBS) -o $@/$@
LINK_ORDER2 = $(CC) $< $(INCLUDE) $(LIB) $(LIBS) -o $@/$@
LINK_ORDER3 = $(CC) $(filter %.o, $^) $(INCLUDE) $(LIB) $(LIBS) -o $@/$@
LINK_ORDER4 = $(CC) $(filter %.o, $^) $(INCLUDE) $(LIB) $(LIBS) -o

%.o : %.c
	$(COMPILE_ORDER)

%.o : %.cpp
	$(COMPILE_ORDER)


GLFramework : GLFramework/GLFramework.o
	ar rc $@/libGLFramework.a $^

MyMathDoodle : MyMathDoodle/MyMathDoodle.o
	ar rc $@/libMyMathDoodle.a $^

test : test/test.o
	$(LINK_ORDER)

02-01_MyFirstGL : 02-01_MyFirstGL/02-01_MyFirstGL.o GLFramework
	$(LINK_ORDER3)

02-02_UsingShader : 02-02_UsingShader/02-02_UsingShader.o GLFramework
	$(LINK_ORDER3)

02-03_DrawTriangle : 02-03_DrawTriangle/02-03_DrawTriangle.o GLFramework
	$(LINK_ORDER3)


03-01_VertexShader : 03-01_VertexShader/03-01_VertexShader.o GLFramework
	$(LINK_ORDER3)

03-02_DataTransformBetweenStage : 03-02_DataTransformBetweenStage/03-02_DataTransformBetweenStage.o GLFramework
	$(LINK_ORDER3)

03-03 = 03-03_TessellationShader
$(03-03) : $(03-03)/main.o GLFramework
	$(LINK_ORDER4) $(03-03)/application

03-04 = 03-04_GeometryShader
$(03-04) : $(03-04)/main.o GLFramework
	$(LINK_ORDER4) $(03-04)/application

03-05 = 03-05_FragmentShader
$(03-05) : $(03-05)/main.o GLFramework
	$(LINK_ORDER4) $(03-05)/application

04-01 = 04-01_DotProduct
$(04-01) : $(04-01)/main.o 
	$(LINK_ORDER4) $(04-01)/application

05-01 = 05-01_Buffer
$(05-01) : $(05-01)/main.o GLFramework
	$(LINK_ORDER4) $(05-01)/application

05-02 = 05-02_StructAndStride
$(05-02) : $(05-02)/main.o GLFramework
	$(LINK_ORDER4) $(05-02)/application

05-03 = 05-03_LoadObjectFile
$(05-03) : $(05-03)/main.o GLFramework
	$(LINK_ORDER4) $(05-03)/application

05-04 = 05-04_DefaultBlockUniform
$(05-04) : $(05-04)/main.o GLFramework
	$(LINK_ORDER4) $(05-04)/application

05-05 = 05-05_UniformBlock
$(05-05) : $(05-05)/main.o GLFramework
	$(LINK_ORDER4) $(05-05)/application
