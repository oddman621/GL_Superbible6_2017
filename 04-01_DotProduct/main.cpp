#include <iostream>
#include <glm/glm.hpp>

//includes for print glm::vec values.
#include <glm/ext.hpp>
#include <glm/gtx/string_cast.hpp>

#include "../MyMathDoodle/MyMathDoodle.hpp"

int main(void)
{
	glm::vec2 x = {2.0f, 3.0f};
	glm::vec2 y = {4.0f, 6.0f};

	MyMathDoodle::vec2<float> mmd_x = {2.0f, 3.0f};
	MyMathDoodle::vec2<float> mmd_y = {4.0f, 6.0f};

	std::cout << " x = " << glm::to_string(x) << std::endl
			  << " y = " << glm::to_string(y) << std::endl
			  << "glm::dot(x,y) = " << glm::dot(x,y)<<std::endl;

	std::cout << " mmd_x = ";

	return 0;
}
