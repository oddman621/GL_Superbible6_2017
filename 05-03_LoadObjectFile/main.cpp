#include "../GLFramework/GLFramework.h"


int main(void)
{
	GLFramework fw;
	GLFWwindow *const window = fw.window;
	GLuint const &vao = fw.vao;

	double deltaSec = 0;
	double prevTime = 0;
	double currTime = 0;

	const std::string
		vs_src = GLFramework::LoadSource("vs.glsl"),
		fs_src = GLFramework::LoadSource("fs.glsl");
	GLuint shaders[] = {
		GLFramework::CompileShader(vs_src.c_str(), GL_VERTEX_SHADER),
		GLFramework::CompileShader(fs_src.c_str(), GL_FRAGMENT_SHADER),

	};
	const int shadersArraySize = sizeof(shaders)/sizeof(*shaders);
		
	GLuint program = GLFramework::CompileProgram(
		shadersArraySize, shaders);

	///user init start
	std::vector<glm::vec4> suzanne_vertices;
	std::vector<glm::vec3> suzanne_uvs;
	std::vector<glm::vec3> suzanne_normals;
	GLFramework::LoadOBJ("suzanne.obj", &suzanne_vertices,
						 &suzanne_uvs, &suzanne_normals);

	GLuint suzanne_vertices_buffer,
		suzanne_uvs_buffer, suzanne_normals_buffer;
	glGenBuffers(1, &suzanne_vertices_buffer);
	glGenBuffers(1, &suzanne_uvs_buffer);
	glGenBuffers(1, &suzanne_normals_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, suzanne_vertices_buffer);
	glBufferData(GL_ARRAY_BUFFER,
				 sizeof(glm::vec4)*suzanne_vertices.size(),
				 &suzanne_vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, suzanne_uvs_buffer);
	glBufferData(GL_ARRAY_BUFFER,
				 sizeof(glm::vec3)*suzanne_uvs.size(),
				 &suzanne_uvs[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, suzanne_normals_buffer);
	glBufferData(GL_ARRAY_BUFFER,
				 sizeof(glm::vec3)*suzanne_normals.size(),
				 &suzanne_normals[0], GL_STATIC_DRAW);
	///user init end

	while(!glfwWindowShouldClose(window))
	{
		///user defined attrib start
		/*
	   	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
						  sizeof(vertex),
						  (void *)offsetof(vertex, pos));
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
						  sizeof(vertex),
						  (void *)offsetof(vertex, color));
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		*/
		glBindBuffer(GL_ARRAY_BUFFER, suzanne_vertices_buffer);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE,
							  0, NULL);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, suzanne_uvs_buffer);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
							  0, NULL);
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, suzanne_normals_buffer);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,
							  0, NULL);
		glEnableVertexAttribArray(2);
		///user defined attrib end

		prevTime = currTime;
		currTime = glfwGetTime();
		deltaSec = currTime - prevTime;

		const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		glClearBufferfv(GL_COLOR, 0, black);

		glUseProgram(program);

		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CCW);
		glCullFace(GL_FRONT);

		glDrawArrays(GL_TRIANGLES, 0, suzanne_vertices.size());
		glfwSwapBuffers(window);
		glfwPollEvents();
		
		if(glfwGetKey(window, GLFW_KEY_ESCAPE))
			glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

	glDeleteProgram(program);
	for(int i=0; i<shadersArraySize; i++)
		glDeleteShader(shaders[i]);

	return 0;
}
